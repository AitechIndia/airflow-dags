from airflow.hooks.http_hook import BaseHook


class RAMerakyGatewayBaseHook(BaseHook):

  def __init__(self, source, ra_conn_id='ra_meraki_gateway'):
    super().__init__(source)
    self.ra_conn_id = ra_conn_id
